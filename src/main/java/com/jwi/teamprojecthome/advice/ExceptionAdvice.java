package com.jwi.teamprojecthome.advice;

import com.jwi.teamprojecthome.enums.ResultCode;
import com.jwi.teamprojecthome.exception.CMissingDataException;
import com.jwi.teamprojecthome.model.CommonResult;
import com.jwi.teamprojecthome.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

// 조언자
@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) { // 기본적인 출구
        return ResponseService.getFailResult(ResultCode.FAILED); // 출구니까 FAILED
        // 감지못한 에러가 발생 시 400 BAD REQUEST 로 출력 기본 출구로 빠져나감
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST) // 사용자 잘못으로 돌리겠다
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }
}
