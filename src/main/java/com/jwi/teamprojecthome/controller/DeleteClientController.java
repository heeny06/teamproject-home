package com.jwi.teamprojecthome.controller;

import com.jwi.teamprojecthome.model.CommonResult;
import com.jwi.teamprojecthome.service.DeleteClientService;
import com.jwi.teamprojecthome.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/delAll")
public class DeleteClientController {
    private final DeleteClientService deleteClientService;

    @DeleteMapping("/{clientId}")
    public CommonResult delClientOwnerAll(@PathVariable long clientId) {
        deleteClientService.delClientOwnerAll(clientId);

        return ResponseService.getSuccessResult();
    }
}
