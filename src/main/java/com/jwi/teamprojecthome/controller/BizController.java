package com.jwi.teamprojecthome.controller;

import com.jwi.teamprojecthome.model.BizItem;
import com.jwi.teamprojecthome.model.BizRequest;
import com.jwi.teamprojecthome.model.CommonResult;
import com.jwi.teamprojecthome.model.ListResult;
import com.jwi.teamprojecthome.service.BizService;
import com.jwi.teamprojecthome.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/biz")
public class BizController {
    private final BizService bizService;

    @PostMapping("/new")
    public CommonResult setBiz(@RequestBody @Valid BizRequest bizRequest) {
        bizService.setBiz(bizRequest.getBizName(), bizRequest.getBizNumber(),
                bizRequest.getBizOwner(), bizRequest.getBizAddress(), bizRequest.getBizPhone());

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    public ListResult<BizItem> getBizs() {
        return ResponseService.getListResult(bizService.getBizs(), true);
    }

    @PutMapping("/{id}")
    public CommonResult putBiz(@PathVariable long id, @RequestBody @Valid BizRequest request) {
        bizService.putBiz(id, request.getBizName(), request.getBizNumber(), request.getBizOwner(),
                request.getBizAddress(), request.getBizPhone());

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{id}")
    public CommonResult delBiz(@PathVariable long id) {
        bizService.delBiz(id);

        return ResponseService.getSuccessResult();
    }
}
