package com.jwi.teamprojecthome.controller;

import com.jwi.teamprojecthome.entity.Biz;
import com.jwi.teamprojecthome.entity.BizInfo;
import com.jwi.teamprojecthome.model.BizInfoItem;
import com.jwi.teamprojecthome.model.BizInfoRequest;
import com.jwi.teamprojecthome.model.CommonResult;
import com.jwi.teamprojecthome.model.ListResult;
import com.jwi.teamprojecthome.service.BizInfoService;
import com.jwi.teamprojecthome.service.BizService;
import com.jwi.teamprojecthome.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/bizInfo")
public class BizInfoController {
    private final BizInfoService bizInfoService;
    private final BizService bizService;

    @PostMapping("/bizId/{bizId}")
    public CommonResult setBizInfo(@PathVariable long bizId, @RequestBody @Valid BizInfoRequest bizInfoRequest) {
        Biz biz = bizService.getBizInfo(bizId);
        bizInfoService.setBizInfo(biz, bizInfoRequest.getExpiryData(), bizInfoRequest.getStage(),
                bizInfoRequest.getPtNum(), bizInfoRequest.getProduct());

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    public ListResult<BizInfoItem> getBizinfos() {
        return ResponseService.getListResult(bizInfoService.getBizInfos(), true);
    }

    @PutMapping("/{id}")
    public CommonResult putBizInfo(@PathVariable long id, @RequestBody @Valid BizInfoRequest bizInfoRequest) {
        bizInfoService.putBizInfo(id, bizInfoRequest.getExpiryData(), bizInfoRequest.getStage(),
                bizInfoRequest.getPtNum(), bizInfoRequest.getProduct());

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/bizInfo/{bizInfoId}/biz/{bizId}")
    public CommonResult putBizInfoOwner(@PathVariable long bizInfoId, @PathVariable long bizId) {
        Biz biz = bizService.getBizInfo(bizId);
        bizInfoService.putBizInfoOwner(bizInfoId, biz);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{id}")
    public CommonResult delBizInfo(@PathVariable long id) {
        bizInfoService.delBizInfo(id);

        return ResponseService.getSuccessResult();
    }

}
