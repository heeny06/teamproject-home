package com.jwi.teamprojecthome.controller;

import com.jwi.teamprojecthome.model.CommonResult;
import com.jwi.teamprojecthome.service.DeleteBizService;
import com.jwi.teamprojecthome.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/delAll")
public class DeleteBizController {
    private final DeleteBizService deleteBizService;

    @DeleteMapping("/biz/{bizId}")
    public CommonResult delBizOwnerAll(@PathVariable long bizId) {
        deleteBizService.delBizAll(bizId);

        return ResponseService.getSuccessResult();
    }
}
