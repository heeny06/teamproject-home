package com.jwi.teamprojecthome.controller;

import com.jwi.teamprojecthome.entity.Client;
import com.jwi.teamprojecthome.model.ClientInfoItem;
import com.jwi.teamprojecthome.model.ClientInfoRequest;
import com.jwi.teamprojecthome.model.CommonResult;
import com.jwi.teamprojecthome.model.ListResult;
import com.jwi.teamprojecthome.service.ClientInfoService;
import com.jwi.teamprojecthome.service.ClientService;
import com.jwi.teamprojecthome.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/clientInfo")
public class ClientInfoController {
    private final ClientService clientService;
    private final ClientInfoService clientInfoService;

    @PostMapping("/client/{clientId}")
    public CommonResult setClientInfo(@PathVariable long clientId, @RequestBody @Valid ClientInfoRequest clientInfoRequest) {
        Client client = clientService.getClientInfo(clientId);
        clientInfoService.setClientInfo
                (client, clientInfoRequest.getExpiryData(), clientInfoRequest.getClientTier(), clientInfoRequest.getClientExp(),
                        clientInfoRequest.getTrouble(), clientInfoRequest.getClientPlan(), clientInfoRequest.getTicket());

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/clientInfoAll")
    public ListResult<ClientInfoItem> getClientInfos() {
        return ResponseService.getListResult(clientInfoService.getClientInfos(), true);
    }

    @PutMapping("/{id}")
    public CommonResult putClientInfo(@PathVariable long id, @RequestBody @Valid ClientInfoRequest clientInfoRequest) {
        clientInfoService.putClientInfo(id, clientInfoRequest.getExpiryData(), clientInfoRequest.getClientTier(),
                clientInfoRequest.getClientExp(), clientInfoRequest.getTrouble(), clientInfoRequest.getClientPlan(), clientInfoRequest.getTicket());

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/clientInfo/{clientInfoId}/client/{clientId}")
    public CommonResult putClientInfoOwner(@PathVariable long clientId, @PathVariable long clientInfoId) {
        Client client = clientService.getClientInfo(clientId);
        clientInfoService.putClientInfoOwner(clientInfoId, client);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{id}")
    public CommonResult delClientInfo(@PathVariable long id) {
        clientInfoService.delClientInfo(id);

        return ResponseService.getSuccessResult();
    }
}
