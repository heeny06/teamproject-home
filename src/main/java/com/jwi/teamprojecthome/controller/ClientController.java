package com.jwi.teamprojecthome.controller;

import com.jwi.teamprojecthome.model.ClientItem;
import com.jwi.teamprojecthome.model.ClientRequest;
import com.jwi.teamprojecthome.model.CommonResult;
import com.jwi.teamprojecthome.model.ListResult;
import com.jwi.teamprojecthome.service.ClientService;
import com.jwi.teamprojecthome.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class
ClientController {
    private final ClientService clientService;

    @PostMapping("/new")
    public CommonResult setClient(@RequestBody @Valid ClientRequest userRequest) {
        clientService.setClient(userRequest.getNickName(), userRequest.getName(),
                userRequest.getGender(), userRequest.getPhone(), userRequest.getAddress());

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    public ListResult<ClientItem> getClients() {
        return ResponseService.getListResult(clientService.getClients(), true);
    }

    @PutMapping("/{id}")
    public CommonResult putClient(@PathVariable long id, @RequestBody @Valid ClientRequest clientRequest) {
        clientService.putClient(id, clientRequest.getNickName(), clientRequest.getName(),
                clientRequest.getPhone(), clientRequest.getAddress(), clientRequest.getGender());

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/{id}")
    public CommonResult delClient(@PathVariable long id) {
        clientService.delUser(id);

        return ResponseService.getSuccessResult();
    }
}
