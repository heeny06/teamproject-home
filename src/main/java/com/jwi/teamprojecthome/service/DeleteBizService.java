package com.jwi.teamprojecthome.service;

import com.jwi.teamprojecthome.entity.BizInfo;
import com.jwi.teamprojecthome.repository.BizInfoRepository;
import com.jwi.teamprojecthome.repository.BizRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeleteBizService {
    private final BizRepository bizRepository;
    private final BizInfoRepository bizInfoRepository;

    public void delBizAll(long bizId) {
        List<BizInfo> bizInfos = bizInfoRepository.findAllByBiz_Id(bizId);
        for (BizInfo bizInfo : bizInfos) {
            bizInfoRepository.deleteById(bizInfo.getId());
        }
        bizRepository.deleteById(bizId);
    }
}
