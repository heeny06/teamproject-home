package com.jwi.teamprojecthome.service;

import com.jwi.teamprojecthome.enums.ResultCode;
import com.jwi.teamprojecthome.model.CommonResult;
import com.jwi.teamprojecthome.model.ListResult;
import com.jwi.teamprojecthome.model.SingleResult;
import org.springframework.stereotype.Service;
@Service
public class ResponseService { // 협업하는 공간이라 RequiredArgsConstructor 없음
    public static <T> ListResult<T> getListResult(ListResult<T> result, boolean isSuccess) {
        if (isSuccess) setSuccessResult(result); // CommonResult 를 상속 받았기 때문에 ListResult 가 보유하고있음
        else setFailResult(result); // 만약에 참이 아닐경우
        return result;
    }

    public static <T> SingleResult<T> getSingleResult(T data) {
    // 무언가 하나를 받을거야 알겠지? > returnType model 에서 그대로 빼옴
    // 무언가 하나를 받을게라고 선언해줬기에 바구니 안에 T를 받을 수 있음
        SingleResult<T> result = new SingleResult<>();
        result.setData(data);
        setSuccessResult(result); // 밑에 Success 에 대한 메소드를 만들어 놨으니 재사용

        return result;

    }

    public static CommonResult getSuccessResult() {
        CommonResult result = new CommonResult();
        setSuccessResult(result);

        return result;
    }
    // Success 일 경우 밑 Success 메소드에서 설정한 Code, Msg 출력
    // enum 추가
    public static CommonResult getFailResult(ResultCode resultCode) {
        CommonResult result = new CommonResult(); // CommonResult 형식의 result 빈 박스 생성
        result.setIsSuccess(false);
        result.setCode(resultCode.getCode());
        result.setMsg(resultCode.getMsg());

        return result;
    }
    // 실패한 경우의 수는 다양하기 때문에 해당되는 오류코드와 메세지를 출력시켜 달라고 하는 것

    private static void setSuccessResult(CommonResult result) {
        // static = 프로그램이 실행되는 동안 메모리에서 고정으로 자리 잡혀있다(매번 사용되기 때문). 반대로 너무 많으면 안 됨
        result.setIsSuccess(true);
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
    }

    private static void setFailResult(CommonResult result) {
        result.setIsSuccess(false);
        result.setCode(ResultCode.FAILED.getCode());
        result.setMsg(ResultCode.FAILED.getMsg());
    }
}
