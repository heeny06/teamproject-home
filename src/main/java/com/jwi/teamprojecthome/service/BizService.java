package com.jwi.teamprojecthome.service;

import com.jwi.teamprojecthome.entity.Biz;
import com.jwi.teamprojecthome.exception.CMissingDataException;
import com.jwi.teamprojecthome.model.BizItem;
import com.jwi.teamprojecthome.model.ListResult;
import com.jwi.teamprojecthome.repository.BizRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BizService {
    private final BizRepository bizRepository;

    public void setBiz(String bizName, String bizNumber, String bizOwner, String bizAddress, String bizPhone) {
        Biz biz = new Biz();
        biz.setBizName(bizName);
        biz.setBizNumber(bizNumber);
        biz.setBizOwner(bizOwner);
        biz.setBizAddress(bizAddress);
        biz.setBizPhone(bizPhone);
        biz.setDateCreate(LocalDateTime.now());
        biz.setDateUpdate(LocalDateTime.now());
        bizRepository.save(biz);
    }

    public ListResult<BizItem> getBizs() {
        List<BizItem> result = new LinkedList<>();
        List<Biz> bizs = bizRepository.findAll();
        for (Biz biz : bizs) {
            BizItem bizItem = new BizItem();
            bizItem.setId(biz.getId());
            bizItem.setBizName(biz.getBizName());
            bizItem.setBizNumber(biz.getBizNumber());
            bizItem.setBizOwner(biz.getBizOwner());
            bizItem.setBizAddress(biz.getBizAddress());
            bizItem.setBizPhone(biz.getBizPhone());
            bizItem.setDateCreate(biz.getDateCreate());
            result.add(bizItem);
        }
        return ListConvertService.settingResult(result);
    }

    public Biz getBizInfo(long id) {
        return bizRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void putBiz(long id, String bizName, String bizNumber, String bizOwner, String bizAddress, String bizPhone) {
        Biz biz = bizRepository.findById(id).orElseThrow(CMissingDataException::new);
        biz.setBizName(bizName);
        biz.setBizNumber(bizNumber);
        biz.setBizOwner(bizOwner);
        biz.setBizAddress(bizAddress);
        biz.setBizPhone(bizPhone);
        biz.setDateUpdate(LocalDateTime.now());
        bizRepository.save(biz);
    }

    public void delBiz(long id) {
        bizRepository.deleteById(id);
    }

}
