package com.jwi.teamprojecthome.service;

import com.jwi.teamprojecthome.entity.Biz;
import com.jwi.teamprojecthome.entity.BizInfo;
import com.jwi.teamprojecthome.enums.Product;
import com.jwi.teamprojecthome.enums.Stage;
import com.jwi.teamprojecthome.exception.CMissingDataException;
import com.jwi.teamprojecthome.model.BizInfoItem;
import com.jwi.teamprojecthome.model.ListResult;
import com.jwi.teamprojecthome.repository.BizInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BizInfoService {
    private final BizInfoRepository bizInfoRepository;

    public void setBizInfo(Biz biz, LocalDate expiryData, Stage stage, Integer ptNum, Product product) {
        BizInfo bizInfo = new BizInfo();
        bizInfo.setBiz(biz);
        bizInfo.setExpiryData(expiryData);
        bizInfo.setStage(stage);
        bizInfo.setPtNum(ptNum);
        bizInfo.setProduct(product);
        bizInfo.setDateCreate(LocalDateTime.now());
        bizInfo.setDateUpdate(LocalDateTime.now());
        bizInfoRepository.save(bizInfo);
    }

    public ListResult<BizInfoItem> getBizInfos() {
        List<BizInfoItem> result = new LinkedList<>();
        List<BizInfo> bizInfos = bizInfoRepository.findAll();
        for (BizInfo bizInfo : bizInfos) {
            BizInfoItem bizInfoItem = new BizInfoItem();
            bizInfoItem.setId(bizInfo.getId());
            bizInfoItem.setExpiryData(bizInfo.getExpiryData());
            bizInfoItem.setStage(bizInfo.getStage().getName());
            bizInfoItem.setPtNum(bizInfo.getPtNum());
            bizInfoItem.setProduct(bizInfo.getProduct().getName());
            bizInfoItem.setDateCreate(bizInfo.getDateCreate());
            bizInfoItem.setBizId(bizInfo.getBiz().getId());
            bizInfoItem.setBizName(bizInfo.getBiz().getBizName());
            bizInfoItem.setBizNumber(bizInfo.getBiz().getBizNumber());
            bizInfoItem.setBizOwner(bizInfo.getBiz().getBizOwner());
            bizInfoItem.setBizAddress(bizInfo.getBiz().getBizAddress());
            bizInfoItem.setBizPhone(bizInfo.getBiz().getBizPhone());
            bizInfoItem.setBizDateCreate(bizInfo.getBiz().getDateCreate());
            result.add(bizInfoItem);
        }
        return ListConvertService.settingResult(result);
    }

    public void putBizInfo(long id, LocalDate expiryData, Stage stage, Integer ptNum, Product product) {
       BizInfo bizInfo = bizInfoRepository.findById(id).orElseThrow(CMissingDataException::new);
       bizInfo.setExpiryData(expiryData);
       bizInfo.setStage(stage);
       bizInfo.setPtNum(ptNum);
       bizInfo.setProduct(product);
       bizInfo.setDateUpdate(LocalDateTime.now());
       bizInfoRepository.save(bizInfo);
    }

    public void putBizInfoOwner(long id, Biz biz) {
        BizInfo bizInfo = bizInfoRepository.findById(id).orElseThrow(CMissingDataException::new);
        bizInfo.setBiz(biz);
        bizInfo.setDateUpdate(LocalDateTime.now());
        bizInfoRepository.save(bizInfo);
    }

    public void delBizInfo(long id) {
        bizInfoRepository.deleteById(id);
    }
}
