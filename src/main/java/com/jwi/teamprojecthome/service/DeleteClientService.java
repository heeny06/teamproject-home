package com.jwi.teamprojecthome.service;

import com.jwi.teamprojecthome.entity.ClientInfo;
import com.jwi.teamprojecthome.repository.ClientInfoRepository;
import com.jwi.teamprojecthome.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeleteClientService {
    private final ClientRepository clientRepository;
    private final ClientInfoRepository clientInfoRepository;

    public void delClientOwnerAll(long clientId) {
        List<ClientInfo> clientInfos = clientInfoRepository.findAllByClient_Id(clientId);

        for (ClientInfo clientInfo : clientInfos) {
            clientInfoRepository.deleteById(clientInfo.getId());
        }

        clientRepository.deleteById(clientId);
    }
}
