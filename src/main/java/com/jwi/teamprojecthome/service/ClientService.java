package com.jwi.teamprojecthome.service;

import com.jwi.teamprojecthome.entity.Client;
import com.jwi.teamprojecthome.enums.Gender;
import com.jwi.teamprojecthome.exception.CMissingDataException;
import com.jwi.teamprojecthome.model.ClientItem;
import com.jwi.teamprojecthome.model.ListResult;
import com.jwi.teamprojecthome.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientService {
    private final ClientRepository clientRepository;

    public void setClient(String nickName, String name, Gender gender, String phone, String address) {
        Client client = new Client();
        client.setNickName(nickName);
        client.setName(name);
        client.setGender(gender);
        client.setPhone(phone);
        client.setAddress(address);
        client.setDateCreate(LocalDateTime.now());
        client.setDateUpdate(LocalDateTime.now());
        clientRepository.save(client);
    }

    public ListResult<ClientItem> getClients() {
        List<ClientItem> result = new LinkedList<>();
        List<Client> clients = clientRepository.findAll();
        for (Client client : clients) {
            ClientItem clientItem = new ClientItem();
            clientItem.setId(client.getId());
            clientItem.setNickName(client.getNickName());
            clientItem.setName(client.getName());
            clientItem.setGender(client.getGender().getName());
            clientItem.setPhone(client.getPhone());
            clientItem.setAddress(client.getAddress());
            result.add(clientItem);
        }
        return ListConvertService.settingResult(result);
    }

    public Client getClientInfo(long id) {
        return clientRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void putClient(long id, String nickName, String name, String phone, String address, Gender gender) {
        Client client = clientRepository.findById(id).orElseThrow(CMissingDataException::new);
        client.setNickName(nickName);
        client.setName(name);
        client.setGender(gender);
        client.setPhone(phone);
        client.setAddress(address);
        clientRepository.save(client);
    }

    public void delUser(long id) {

        clientRepository.deleteById(id);
    }
}
