package com.jwi.teamprojecthome.service;

import com.fasterxml.jackson.databind.deser.CreatorProperty;
import com.jwi.teamprojecthome.entity.Client;
import com.jwi.teamprojecthome.entity.ClientInfo;
import com.jwi.teamprojecthome.enums.ClientTier;
import com.jwi.teamprojecthome.enums.Trouble;
import com.jwi.teamprojecthome.exception.CMissingDataException;
import com.jwi.teamprojecthome.model.ClientInfoItem;
import com.jwi.teamprojecthome.model.ListResult;
import com.jwi.teamprojecthome.repository.ClientInfoRepository;
import com.jwi.teamprojecthome.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientInfoService {
    private final ClientInfoRepository clientInfoRepository;

    public void setClientInfo(Client client, LocalDate expiryData, ClientTier clientTier, String clientExp,
                              Trouble trouble, String clientPlan, String ticket) {
        ClientInfo clientInfo = new ClientInfo();
        clientInfo.setClient(client);
        clientInfo.setExpiryData(expiryData);
        clientInfo.setClientTier(clientTier);
        clientInfo.setClientExp(clientExp);
        clientInfo.setTrouble(trouble);
        clientInfo.setClientPlan(clientPlan);
        clientInfo.setTicket(ticket);
        clientInfo.setDateCreate(LocalDateTime.now());
        clientInfo.setDateUpdate(LocalDateTime.now());
        clientInfoRepository.save(clientInfo);
    }

    public ListResult<ClientInfoItem> getClientInfos() {
        List<ClientInfoItem> result = new LinkedList<>();
        List<ClientInfo> clientInfos = clientInfoRepository.findAll();
        for (ClientInfo clientInfo : clientInfos) {
            ClientInfoItem clientInfoItem = new ClientInfoItem();
            clientInfoItem.setId(clientInfo.getId());
            clientInfoItem.setExpiryData(clientInfo.getExpiryData());
            clientInfoItem.setUserTier(clientInfo.getClientTier().getName());
            clientInfoItem.setClientExp(clientInfo.getClientExp());
            clientInfoItem.setTrouble(clientInfo.getTrouble().getName());
            clientInfoItem.setUserPlan(clientInfo.getClientPlan());
            clientInfoItem.setTicket(clientInfo.getTicket());
            clientInfoItem.setNickName(clientInfo.getClient().getNickName());
            clientInfoItem.setClientName(clientInfo.getClient().getName());
            clientInfoItem.setGender(clientInfo.getClient().getGender().getName());
            clientInfoItem.setPhone(clientInfo.getClient().getPhone());
            clientInfoItem.setAddress(clientInfo.getClient().getAddress());
            clientInfoItem.setDateCreate(clientInfo.getDateCreate());
            result.add(clientInfoItem);
        }
        return ListConvertService.settingResult(result);
    }

    public void putClientInfo(long id, LocalDate expiryData, ClientTier clientTier, String clientExp,
                              Trouble trouble, String clientPlan, String ticket) {
        ClientInfo clientInfo = clientInfoRepository.findById(id).orElseThrow(CMissingDataException::new);
        clientInfo.setExpiryData(expiryData);
        clientInfo.setClientTier(clientTier);
        clientInfo.setClientExp(clientExp);
        clientInfo.setTrouble(trouble);
        clientInfo.setClientPlan(clientPlan);
        clientInfo.setTicket(ticket);
        clientInfo.setDateUpdate(LocalDateTime.now());
        clientInfoRepository.save(clientInfo);
    }

    public void putClientInfoOwner(long clientId, Client client) {
        ClientInfo clientInfo = clientInfoRepository.findById(clientId).orElseThrow(CMissingDataException::new);
        clientInfo.setClient(client);
        clientInfo.setDateUpdate(LocalDateTime.now());
        clientInfoRepository.save(clientInfo);
    }

    public void delClientInfo(long id) {
        clientInfoRepository.deleteById(id);
    }
}
