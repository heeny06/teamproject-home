package com.jwi.teamprojecthome.exception;
// Custom
public class CMissingDataException extends RuntimeException{ // 출구니까 출구의 기능을 상속 받아야함
    public CMissingDataException(String msg, Throwable t) { // 생성자 3개
        super(msg, t);
    }

    public CMissingDataException(String msg) {
        super(msg);
    }

    public CMissingDataException() {
        super();
    }

}
