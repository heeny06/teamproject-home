package com.jwi.teamprojecthome.entity;

import com.jwi.teamprojecthome.enums.ClientTier;
import com.jwi.teamprojecthome.enums.Trouble;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ClientInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clientId", nullable = false)
    private Client client;

    @Column(nullable = false)
    private LocalDate expiryData;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ClientTier clientTier;

    @Column(nullable = false, length = 20)
    private String clientExp;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Trouble trouble;

    @Column(nullable = false, length = 20)
    private String clientPlan;

    @Column(nullable = false, length = 15)
    private String ticket;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

}
