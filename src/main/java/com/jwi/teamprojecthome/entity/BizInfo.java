package com.jwi.teamprojecthome.entity;

import com.jwi.teamprojecthome.enums.Product;
import com.jwi.teamprojecthome.enums.Stage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class BizInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bizId", nullable = false)
    private Biz biz;

    @Column(nullable = false)
    private LocalDate expiryData;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Stage stage;

    @Column(nullable = false)
    private Integer ptNum;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Product product;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

}
