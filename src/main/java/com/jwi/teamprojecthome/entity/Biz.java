package com.jwi.teamprojecthome.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Biz {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String bizName;

    @Column(nullable = false, length = 13)
    private String bizNumber;

    @Column(nullable = false, length = 20)
    private String bizOwner;

    @Column(nullable = false, length = 50)
    private String bizAddress;

    @Column(nullable = false, length = 20)
    private String bizPhone;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

}
