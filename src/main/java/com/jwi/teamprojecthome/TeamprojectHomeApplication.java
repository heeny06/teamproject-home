package com.jwi.teamprojecthome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeamprojectHomeApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeamprojectHomeApplication.class, args);
    }

}
