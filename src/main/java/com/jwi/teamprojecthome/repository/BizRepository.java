package com.jwi.teamprojecthome.repository;

import com.jwi.teamprojecthome.entity.Biz;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BizRepository extends JpaRepository<Biz, Long> {
}
