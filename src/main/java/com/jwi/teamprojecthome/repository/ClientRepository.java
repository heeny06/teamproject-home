package com.jwi.teamprojecthome.repository;

import com.jwi.teamprojecthome.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {
}
