package com.jwi.teamprojecthome.repository;

import com.jwi.teamprojecthome.entity.BizInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BizInfoRepository extends JpaRepository<BizInfo, Long> {
    List<BizInfo> findAllByBiz_Id(long id);
}
