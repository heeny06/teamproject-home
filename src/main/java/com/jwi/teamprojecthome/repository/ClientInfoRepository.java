package com.jwi.teamprojecthome.repository;

import com.jwi.teamprojecthome.entity.ClientInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClientInfoRepository extends JpaRepository<ClientInfo, Long> {
    List<ClientInfo> findAllByClient_Id(long id);
}
