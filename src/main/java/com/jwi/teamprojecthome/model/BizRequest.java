package com.jwi.teamprojecthome.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BizRequest {
    @NotNull
    @Length(min = 1, max = 20)
    private String bizName;

    @NotNull
    @Length(min = 1, max = 13)
    private String bizNumber;

    @NotNull
    @Length(min = 1, max = 20)
    private String bizOwner;

    @NotNull
    @Length(min = 1, max = 50)
    private String bizAddress;

    @NotNull
    @Length(min = 1, max = 20)
    private String bizPhone;
}
