package com.jwi.teamprojecthome.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientItem {
    private Long id;

    private String nickName;

    private String name;

    private String gender;

    private String phone;

    private String address;
}
