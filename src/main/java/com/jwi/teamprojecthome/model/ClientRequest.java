package com.jwi.teamprojecthome.model;

import com.jwi.teamprojecthome.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ClientRequest {

    @NotNull
    @Length(min = 1, max = 30)
    private String nickName;

    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @NotNull
    @Length(min = 10, max = 30)
    private String phone;

    @NotNull
    @Length(min = 10, max = 50)
    private String address;
}
