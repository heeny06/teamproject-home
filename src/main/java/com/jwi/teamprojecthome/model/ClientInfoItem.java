package com.jwi.teamprojecthome.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class ClientInfoItem {

    private Long id;

    private LocalDate expiryData;

    private String userTier;

    private String clientExp;

    private String trouble;

    private String userPlan;

    private String ticket;

    private String nickName;

    private String clientName;

    private String gender;

    private String phone;

    private String address;

    private LocalDateTime dateCreate;
}
