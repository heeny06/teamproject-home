package com.jwi.teamprojecthome.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class BizItem {
    private Long id;

    private String bizName;

    private String bizNumber;

    private String bizOwner;

    private String bizAddress;

    private String bizPhone;

    private LocalDateTime dateCreate;
}
