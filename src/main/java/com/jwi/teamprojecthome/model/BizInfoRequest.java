package com.jwi.teamprojecthome.model;

import com.jwi.teamprojecthome.enums.Product;
import com.jwi.teamprojecthome.enums.Stage;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class BizInfoRequest {
    @NotNull
    private LocalDate expiryData;

    @Enumerated(EnumType.STRING)
    private Stage stage;

    @NotNull
    private Integer ptNum;

    @Enumerated(EnumType.STRING)
    private Product product;
}
