package com.jwi.teamprojecthome.model;

import com.jwi.teamprojecthome.enums.ClientTier;
import com.jwi.teamprojecthome.enums.Trouble;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class ClientInfoRequest {
    @NotNull
    private LocalDate expiryData;

    @Enumerated(EnumType.STRING)
    private ClientTier clientTier;

    @NotNull
    @Length(min = 2, max = 20)
    private String clientExp;

    @Enumerated(EnumType.STRING)
    private Trouble trouble;

    @NotNull
    @Length(min = 2, max = 20)
    private String clientPlan;

    @NotNull
    @Length(min = 2, max = 15)
    private String ticket;
}
