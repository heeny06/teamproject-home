package com.jwi.teamprojecthome.model;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class BizInfoItem {
    private Long id;

    private LocalDate expiryData;

    private String stage;

    private Integer ptNum;

    private String product;

    private LocalDateTime dateCreate;

    private Long bizId;

    private String bizName;

    private String bizNumber;

    private String bizOwner;

    private String bizAddress;

    private String bizPhone;

    private LocalDateTime bizDateCreate;
}
