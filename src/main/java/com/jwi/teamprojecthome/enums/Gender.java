package com.jwi.teamprojecthome.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public enum Gender {
    MAN("남자"),

    WOMAN("여자");

    private final String name;
}
