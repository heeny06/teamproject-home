package com.jwi.teamprojecthome.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Product {
    YOGA("요가"),
    PILATES("필라테스"),
    SPINNING("스피닝"),
    PT("피티"),
    HEALTH("헬스");

    private final String name;
}
