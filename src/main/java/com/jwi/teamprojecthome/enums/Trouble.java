package com.jwi.teamprojecthome.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Trouble {
    YES("질환있음"),
    NO("질환없음");

    private final String name;
}
